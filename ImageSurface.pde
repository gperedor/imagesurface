import processing.video.*;
Capture cam;

int picHeight = 256;
int picWidth = 256;
void setup() {

  String[] cameras = Capture.list();

  if (cameras.length == 0) {
    println("There are no cameras available for capture.");
    exit();
  }
  cam = new Capture(this, picHeight, picWidth);
  cam.start();     

  size(512, 512, P3D);
}

void draw() {
  // Fetch the image from physical camera
  if (cam.available()) {
    cam.read();
    cam.loadPixels();
  } else {
    return;
  }


  // Get viewport camera rotation from user
  background(60);
  beginCamera();
  camera();
  float centredXRot = PI*((mouseX*1.0-width/2)/width);
  float centredYRot = PI*((mouseY*1.0-height/2)/height);
  text("XRot: " + centredXRot, 400, 10);
  text("YRot: " + centredYRot, 400, 30);
  text("Pixels: " + cam.pixels.length, 400, 50);


  translate(width/2, height/2);
  rotateX(centredXRot);
  rotateY(centredYRot);
  rotateZ(PI/2);
  endCamera();

  for (int i = 0; i < cam.width * cam.height; i++) {
    color c = cam.pixels[i];
    float b = brightness(c);
    int x = i/cam.width - cam.width/2;
    int y = i%cam.width - cam.height/2; 
    float h = b*cam.height/(2*255);
    stroke(c);
    point(x, y, h);
  }
}